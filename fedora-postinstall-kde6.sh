#!/bin/bash

#set -e # exit script on error

if [ -z $SUDO_USER ]; then
    echo "Please run this script with sudo"
    exit
fi

function fedoraKde6 {

  dnf dnf distro-sync -y

  dnf install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm -y
  dnf config-manager --enable fedora-cisco-openh264 -y

  dnf install -y ffmpeg --allowerasing

  if [[ $(glxinfo | grep -E "OpenGL vendor|OpenGL renderer") == *"AMD"* ]]; then
    dnf install -y mesa-demos vulkan-tools libva-vdpau-driver

  elif [[ $(glxinfo | grep -E "OpenGL vendor|OpenGL renderer") == *"Intel"* ]]; then
    dnf install -y mesa-demos vulkan-tools libva-intel-media-driver

  elif [[ $(glxinfo | grep -E "OpenGL vendor|OpenGL renderer") == *"NVIDIA"* ]]; then
    dnf install -y akmod-nvidia nvidia-settings
  fi

  dnf install -y texlive-noto-emoji texlive-droid kalk flatseal net-tools
  #dnf install -y timeshift

  dnf install -y system-config-printer
  systemctl enable cups
  firewall-cmd --permanent --add-port=631/tcp

  dnf remove -y  elisa-player dragon kmahjongg kmines kpat
  dnf install -y chromium qbittorrent haruna cpu-x kdenlive obs-studio
  dnf install -y p7zip unrar

  dnf install -y steam
  dnf install -y gamemode lutris goverlay mangohud ### https://github.com/lutris/docs/blob/master/WineDependencies.md ## vkBasalt
  usermod -aG gamemode $SUDO_USER
  
  ### sysctl -a | grep -E "vm.max_map_count"
  #bash -c 'echo "vm.max_map_count=16777216" >> /etc/sysctl.d/99-sysctl.conf'

  dnf remove -y libreoffice-data kcalc

  flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
  flatpak install -y org.onlyoffice.desktopeditors
  flatpak install -y com.github.wwmm.easyeffects
  flatpak install -y com.heroicgameslauncher.hgl
  flatpak install -y net.davidotek.pupgui2
  flatpak install -y com.dropbox.Client
  flatpak install -y it.mijorus.gearlever
  flatpak install -y com.leinardi.gst

  ### DEV
  #dnf install dnf-plugins-core
  #dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
  #dnf install docker-ce docker-ce-cli containerd.io
  #flatpak install -y com.visualstudio.code-oss com.google.AndroidStudio rest.insomnia.Insomnia

  echo "[Desktop Entry]
      Name=Trash
      Name[pt_BR]=Lixeira
      Comment=Contains deleted files
      Comment[pt_BR]=Contém arquivos deletados
      Icon=user-trash-full
      EmptyIcon=user-trash
      Type=Link
      URL=trash:/" > /home/$SUDO_USER/trash.desktop

  echo '# Add in ~/.bashrc or ~/.bash_profile
      function parse_git_branch () {
        git branch 2> /dev/null | sed -e "/^[^*]/d" -e "s/* \(.*\)/(\1)/"
      }

      RED="\[\033[01;31m\]"
      YELLOW="\[\033[01;33m\]"
      GREEN="\[\033[01;32m\]"
      BLUE="\[\033[01;34m\]"
      NO_COLOR="\[\033[00m\]"

      # without host
      # PS1="$GREEN\u$NO_COLOR:$BLUE\w$YELLOW\$(parse_git_branch)$NO_COLOR\$ "
      # with host
      PS1="$GREEN\u@\h$NO_COLOR:$BLUE\w$YELLOW\$(parse_git_branch)$NO_COLOR\$ "' >> /home/$SUDO_USER/.bashrc


  ## FILESHARING
  dnf install -y kdenetwork-filesharing
  firewall-cmd --permanent --add-service=samba
  mkdir /var/lib/samba/usershares
  groupadd -r sambashare
  chown root:sambashare /var/lib/samba/usershares
  chmod 1770 /var/lib/samba/usershares

  echo "[global]
    workgroup = WORKGROUP
    server string %h server (Samba, Fedora)
    log file = /var/log/samba/log.%m
    max log size = 1000
    logging = file
    panic action = /usr/share/samba/panic-action %d
    server role = standalone server
    obey pam restrictions = yes
    unix password sync = yes
    passwd program = /usr/bin/passwd %u
    passwd chat = *Enter\snew\s*\spassword:* %n\n *Retype\snew\s*\spassword:* %n\n *password\supdated\ssucessfully* .
    pam password change = yes
    map to guest = bad user
    usershare allow guests = yes
    usershare max shares = 100
    usershare owner only = true
    usershare path = /var/lib/samba/usershares
    printing = CUPS

    [printers]
    comment = All Printers
    browseable = no
    path = /var/tmp
    printable = yes
    guest ok = no
    read only = yes
    create mask = 0700

    [print$]
    comment = Printer Drivers
    path = /var/lib/samba/printers
    browseable = yes
    read only = yes
    guest ok = no" > /etc/samba/smb.conf


  gpasswd sambashare -a $SUDO_USER
  #usermod -aG usershares $SUDO_USER

  #testparm

  systemctl enable smb

  #qdbus org.kde.plasmashell /PlasmaShell evaluateScript "lockCorona(true)"

  reboot
}

fedoraKde6
